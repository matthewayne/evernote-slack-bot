 #Evernote imports
from evernote.api.client import EvernoteClient # for connecting to the Evenrote API
import evernote.edam.type.ttypes as Types # for note and tags datatypes types
import evernote.edam.notestore.ttypes as NoteStoreTypes # for note filter and result spec datatypes

#Flask Imports
from flask import Flask, render_template, request, session, redirect, url_for, g

#Other Imports
import requests # to get note thumnails through the HTTP API 
import sqlite3
import json
from flask import g
import keys

DATABASE = 'bote.db'

APP_URL = 'https://524fdf04.ngrok.io'

# PORT FOR DEVELOPERMENT SERVER
port=8002

# Evernote Authenticaiton and configuration information
EN_CONSUMER_KEY = keys.evernote_oauth_key
EN_CONSUMER_SECRET= keys.evernote_oauth_secret
sandbox = True #if True will use sandbox.evernote.com, if False will use www.evenrote.com

# Slack Authenticaiton and configuration information
SLACK_KEY = keys.slack_oauth_key
SLACK_SECRET = keys.slack_oauth_secret
SLACK_AUTH_URL = "https://slack.com/oauth/authorize"
SLACK_SCOPE = "teams:read,users:read,chat:write:user,groups:write,bot,commands"

#if sandbox True will use sandbox.evernote.com, if False will use www.evenrote.com
if sandbox:
	EN_URL="https://sandbox.evernote.com"
else:
	EN_URL="https://www.evernote.com"

#Start Flask 
app=Flask(__name__)
app.config['SECRET_KEY'] = keys.app_key #configure the secret key for session data


@app.route("/")
def landing_page():
	payload = {'client_id': SLACK_KEY, 'scope': SLACK_SCOPE, 
		"redirect_uri": APP_URL + "/slack_auth_callback",
		"state":"other"
	}
	requests.get(SLACK_AUTH_URL, params=payload)

	redirect_url = SLACK_AUTH_URL + "?client_id=%s&scope=%s&redirect_uri=%s&state=%s" % (
		SLACK_KEY,
		SLACK_SCOPE,
		APP_URL + "/slack_auth_callback",
		"bote"
	)
	return '<a href="%s">Authorize Bote</a>' % redirect_url


@app.route("/slack_auth_callback")
def slack_auth_callback():
	#code=96865278471.120863822035.a18b109668&state=other
	code = request.args.get('code')
	state = request.args.get('state')
	
	SLACK_URL = "https://slack.com/api/oauth.access"
	payload = {
		"client_id": SLACK_KEY,
		"client_secret":SLACK_SECRET,
		"code":code,
		"redirect_uri":APP_URL + "/slack_auth_callback",
	}
	r = requests.post(SLACK_URL, params=payload)
	auth_info = r.json()

	print auth_info

	slack_user_id = auth_info["user_id"]
	slack_access_token = auth_info["access_token"]
	slack_bot_id = auth_info['bot']['bot_user_id']
	slack_bot_access_token = auth_info['bot']['bot_access_token']
	slack_scope = auth_info["scope"]
	slack_team_name = auth_info["team_name"]
	slack_team_id = auth_info["team_id"]

	client = EvernoteClient(
		consumer_key=EN_CONSUMER_KEY,
		consumer_secret=EN_CONSUMER_SECRET,
		sandbox= sandbox)	
	en_request_token = client.get_request_token(APP_URL+"/evernote_auth_callback")
	en_auth_url = client.get_authorize_url(en_request_token)
	evernote_oauth_token = en_request_token['oauth_token'] #Set OAuth token in the broswer session
	evernote_oauth_secret = en_request_token['oauth_token_secret'] #Set OAuth secret in the broswer session

	conn = get_db()
	c = conn.cursor()

	inputting = [ slack_user_id, slack_access_token, slack_bot_id, slack_bot_access_token, slack_scope, slack_team_name, slack_team_id, evernote_oauth_token, evernote_oauth_secret ]
	sql = "INSERT INTO users (slack_user_id, slack_access_token, slack_bot_id, slack_bot_access_token, slack_scope, slack_team_name, slack_team_id, evernote_oauth_token, evernote_oauth_secret) VALUES (?,?,?,?,?,?,?,?,?)"
	c.execute(sql, inputting)
	conn.commit()

	payload = {
		"token":slack_bot_access_token,
		"channel":slack_user_id,
		"text": "Authorize Bote to access your Evernote account for Bote to work: %s" % en_auth_url,
    	"attachments" : [
        {
            "fallback": "Please authorize Bote to access your Evernote account: %s" % en_auth_url,
            "color": "#36a64f",
            "author_link": "http://flickr.com/bobby/",
            "author_icon": "http://flickr.com/icons/bobby.jpg",
            "title": "Authorize Bote to access Evernote",
            "title_link": "%s" % en_auth_url,
            "text": "Authorize Bote to access your Evernote account for Bote to work",
            "footer": "Bote",
            "footer_icon": "https://platform.slack-edge.com/img/default_application_icon.png",
            "ts": 123456789,
            "username":"Bote",
            "as_user":False
        }]
	}	
	r = requests.post("https://slack.com/api/chat.postMessage", params=payload)

	
	return str(r.json())

@app.route("/evernote_auth_callback")
def enlanding_page():
	#check callback parameters
	if not "oauth_token" in request.args:
		return "Invalid Request"
	if not "oauth_verifier" in request.args:
		return "you didn't authorize Bote to access your Evernote account"
	evernote_oauth_token = request.args.get("oauth_token")
	evernote_oauth_verifier = request.args.get("oauth_verifier")

	# Get temporary Evernote OAuth secret
	conn = get_db()
	c = conn.cursor()
	sql = "SELECT slack_user_id, slack_bot_access_token, evernote_oauth_secret FROM users WHERE evernote_oauth_token = ?"
	c.execute(sql, [ evernote_oauth_token ] )
	( slack_user_id, slack_bot_access_token, evernote_oauth_secret ) = c.fetchone()

	# Get Evernote Access Token
	client = EvernoteClient(
		consumer_key=EN_CONSUMER_KEY,
		consumer_secret=EN_CONSUMER_SECRET,
		sandbox= sandbox)	
	evernote_access_token = client.get_access_token( evernote_oauth_token, evernote_oauth_secret, evernote_oauth_verifier )

	# Write Evernote Access Token to DB
	sql = "UPDATE users SET evernote_access_token = ? WHERE evernote_oauth_token = ?"
	c.execute(sql, [ evernote_access_token, evernote_oauth_token ] )
	conn.commit()

	# Send Slack message to user to indicate auth has succeded
	payload = {
		"token":slack_bot_access_token,
		"channel":slack_user_id,
		"text": "Thank you for authorizing Bote to access your Evernote account!",
    	"attachments": [
        {
            "text": "Thank you for authorizing Bote to access your Evernote account!",
            "footer": "Bote",
            "footer_icon": "https://platform.slack-edge.com/img/default_application_icon.png",
            "ts": 123456789,
            "username":"Bote",
            "as_user":False
        }]
	}	
	r = requests.post("https://slack.com/api/chat.postMessage", params=payload)

	# Send Slack message to user tell the use what they can do with the bot
	payload = {
		"token":slack_bot_access_token,
		"channel":slack_user_id,
		"text": "You can do cool stuff with bote!",
    	"attachments": [
        {
            "text": "You can do cool stuff with bote!",
            "footer": "Bote",
            "footer_icon": "https://platform.slack-edge.com/img/default_application_icon.png",
            "ts": 123456789,
            "username":"Bote",
            "as_user":False
        }]
	}	
	r = requests.post("https://slack.com/api/chat.postMessage", params=payload)

	return "DONE!"

@app.route("/bote_slash", methods=['POST','GET'])
def auth():
	'''
	token=gIkuvaNzQIHg97ATvDxqgjtO
	team_id=T0001
	team_domain=example
	channel_id=C2147483705
	channel_name=test
	user_id=U2147483697
	user_name=Steve
	command=/weather
	text=94070
	response_url=https://hooks.slack.com/commands/1234/5678'''
	'''
	ImmutableMultiDict([('user_id', u'U2URF86K1'), ('response_url', u'https://hooks.slack.com/commands/T2URF86DV/130847049957/B2Wt8fU5odho6sLADOF5faLW'), ('text', u'fjdkfljd'), ('token', u'U8fv8pPPDtNXxSwePfOCU0Yw'), ('channel_id', u'D2UR44XJ4'), ('team_id', u'T2URF86DV'), ('command', u'/bote'), ('team_domain', u'matthewayne'), ('user_name', u'matthewayne'), ('channel_name', u'directmessage')])'''
	
	# Get Slack user ID
	slack_user_id = request.form.get('user_id')

	# Get Evernote access token
	conn = get_db()
	c = conn.cursor()
	sql = "SELECT evernote_access_token FROM users WHERE slack_user_id = ?"
	c.execute(sql, [ slack_user_id ] )
	evernote_access_token = c.fetchone()[0]

	# Check that Evernote Auth works
	try:
		client = EvernoteClient(token=evernote_access_token, sandbox=sandbox)
		note_store = client.get_note_store()
		notebooks = note_store.listNotebooks()
	except:



	# Get commend
	command = request.form.get('text')

	# Get Evernote Data
	if command == "list notebooks":
		notebook_string = ''
		for n in notebooks:
		    notebook_string += n.name + '\n'
		return notebook_string

	
	# Get Evernote Data
	if command[:7] == "search ":
		search_query = command[7:] 
		filter = NoteStoreTypes.NoteFilter()
		filter.words = search_query
		spec = NoteStoreTypes.NotesMetadataResultSpec()
		spec.includeTitle = True
		client = EvernoteClient(token=evernote_access_token, sandbox=sandbox)
		note_store = client.get_note_store()
		user_store = client.get_user_store()
		user = user_store.getUser()
		serach_results = note_store.findNotesMetadata(filter, 0, 10, spec)
		result_string = 'Results for query \'%s\'...\n' % search_query

		for note in serach_results.notes:
			note_url = "%s/shard/%s/nl/%s/%s/" % (EN_URL, user.shardId, user.id, note.guid)
			result_string = "%s :: %s" % (note.title, note_url)
		return result_string


	return "Sorry I can't do that"

def evernote_reauth(slack_user_id):
	client = EvernoteClient(
		consumer_key=EN_CONSUMER_KEY,
		consumer_secret=EN_CONSUMER_SECRET,
		sandbox= sandbox)	
	en_request_token = client.get_request_token(APP_URL+"/evernote_auth_callback")
	en_auth_url = client.get_authorize_url(en_request_token)
	evernote_oauth_token = en_request_token['oauth_token'] #Set OAuth token in the broswer session
	evernote_oauth_secret = en_request_token['oauth_token_secret'] #Set OAuth secret in the broswer session


def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
    return db

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()

if __name__=="__main__":
	app.secret_key = keys.app_key #configure the secret key for session data
	app.run(host='0.0.0.0', debug=True, port=port) #Start the app on the localhost in debug mode on the port specified in the begining of this file
